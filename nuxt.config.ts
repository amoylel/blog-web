// https://nuxt.com/docs/api/configuration/nuxt-config
import prismjs from 'vite-plugin-prismjs'
export default defineNuxtConfig({
    app: {
        head: {
            htmlAttrs: {
                lang: 'zh-CN'
            },
            script: [
                { src: '/scripts/three.min.js', tagPosition: 'bodyClose' },
                { src: '/scripts/vanta.birds.min.js', tagPosition: 'bodyClose' }
            ],
            meta: [
                { charset: 'utf-8' },
                { name: 'viewport', content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' },
                { name: 'apple-mobile-web-app-capable', content: 'yes' }
            ]
        }
    },
    css: [
        // '@/assets/styles/_params.css',
        '@/assets/styles/_font.css',
        '@/assets/styles/_reset.css',
        '@/assets/styles/header-footer.css'
    ],
	devtools: { enabled: true },
	devServer: {
		port: 5000
	},
	nitro: {
		routeRules: {
			'/api/**': {
				proxy: 'http://localhost:3000/**'
			}
		}
	},
	vite: {
		plugins: [
			prismjs({
				languages: [
					'css',
					'sass',
					'scss',
					'javascript',
					'html',
					'swift',
					'xml',
					'typescript',
					'dart',
					'csharp',
					'json',
					'sql',
					'bash',
					'md',
					'nginx',
					'cpp',
					'java'
				],
				plugins: ['toolbar', 'show-language', 'copy-to-clipboard'],
				theme: 'tomorrow',
				css: true
			})
		]
	},
    experimental: {
        externalVue: false,
        inlineSSRStyles: false
    }
})
