import RSS from "rss"

export default defineEventHandler(async (event) => {
    try {
        const response = await fetch('https://yanyunfeng.com/api/web-article?pageIndex=1&pageSize=20')
        if (!response.ok) {
            throw new Error(response?.status?.toString())
        }
        const result = await response.json()
        const feed = new RSS({
            title: '鄢云峰的个人网站',
            site_url: 'https://yanyunfeng.com',
            feed_url: 'https://yanyunfeng.com/rss',
			image_url: 'https://yanyunfeng.com/favicon.ico',
			description: '鄢云峰，一个技术不咋地，头却要秃了的程序员：）',
			copyright: 'Copyright © 2023 鄢云峰 YYF',
			webMaster: '鄢云峰',
			managingEditor: '鄢云峰',
			language: 'zh-cn'
        });
        for (const post of result.data.list) {
            feed.item({
                title: post.title,
                url: `https://yanyunfeng.com/article/${post.id}`,
                description: post.intro,
                date: post.createTime,
                categories: [post.categoryName],
				author: '鄢云峰'
            })
        }
        const feedString = feed.xml({ indent: true })
        event.node.res.setHeader('content-type', 'text/xml')
        event.node.res.end(feedString)
    } catch (e) {
        return e
    }
});